﻿using System;

namespace buildxact_supplies.Validators
{
    public class ValidationException : Exception
    {
        public ValidationException()
        {

        }

        public ValidationException(string message): base(message)
        {
            
        }
    }
}
