﻿using System.IO;

namespace buildxact_supplies.Validators
{
    public class CsvFileExtensionValidator : IValidationRules
    {
        private readonly string fileName;

        public CsvFileExtensionValidator(string fileName)
        {
            this.fileName = fileName;
        }

        public bool Valid()
        {
            return Path.GetExtension(fileName) == ".csv";
        }
    }
}
