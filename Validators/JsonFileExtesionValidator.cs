﻿using System.IO;

namespace buildxact_supplies.Validators
{
    public class JsonFileExtesionValidator : IValidationRules
    {
        private readonly string fileName;

        public JsonFileExtesionValidator(string fileName)
        {
            this.fileName = fileName;
        }

        public bool Valid()
        {
            return Path.GetExtension(fileName) == ".json";
        }
    }
}
