﻿namespace buildxact_supplies.Validators
{
    public interface IValidationRules
    {
        bool Valid();
    }
}
