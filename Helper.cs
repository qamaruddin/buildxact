﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace buildxact_supplies
{
    public static class Helper
    {
        public static string SearchFileInAssemblyByName(string fileName) 
        {
            if (string.IsNullOrEmpty(fileName))
            {
                throw new ArgumentNullException("Must provide a file name.");
            }

            var assem = Assembly.GetExecutingAssembly();
            var file = assem.GetManifestResourceNames().FirstOrDefault(str => str.EndsWith(fileName));
            if (string.IsNullOrEmpty(file))
            {
                return string.Empty;
            }
            using Stream stream = assem.GetManifestResourceStream(file);
            using var reader = new StreamReader(stream);
            return reader.ReadToEnd();
        }

        public static Stream AsStream(this string value)
        {
            var bytes = Encoding.UTF8.GetBytes(value);
            return new MemoryStream(bytes);
        }
    }
}
