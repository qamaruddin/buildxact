﻿using buildxact_supplies.Services;
using buildxact_supplies.Validators;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SuppliesPriceLister
{
    class Program
    {
        private const string MEGACORP_FILE_NAME = "megacorp.json";
        private const string HUMPHRIES_FILE_NAME = "humphries.csv";

        static void Main(string[] args)
        {
            IConfiguration configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", true, true)
                .Build();
            var exchangeRateSection = configuration.GetSection("audUsdExchangeRate").Value;
            if (decimal.TryParse(exchangeRateSection, out decimal exchangeRate))
            {
                var jsonFileValidator  = new JsonFileExtesionValidator(MEGACORP_FILE_NAME);
                var megacorpProcessor = new MegacorpSupplyInfoProcessor(MEGACORP_FILE_NAME, exchangeRate, new List<IValidationRules>() { jsonFileValidator });
                var megacorpData = megacorpProcessor.ProcessSupplyInformation();

                var csvFileValidator = new CsvFileExtensionValidator(HUMPHRIES_FILE_NAME);
                var humphriesProcessor = new HumphriesSupplyInfoProcessor(HUMPHRIES_FILE_NAME, new List<IValidationRules>() { csvFileValidator });
                var humphriesData = humphriesProcessor.ProcessSupplyInformation();

                var orderedSupplyData = megacorpData.Concat(humphriesData).OrderByDescending(x => x.Price);

                foreach (var supply in orderedSupplyData)
                {
                    Console.WriteLine($"{supply.Id} - {supply.ItemName} - {supply.Price}");
                }
            }
            Console.ReadKey();
        }
    }
}
