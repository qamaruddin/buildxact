﻿namespace buildxact_supplies.ViewModels
{
    public class SupplyViewModel
    {
        public string Id { get; set; }
        public string ItemName { get; set; }
        public decimal Price { get; set; }
    }
}
