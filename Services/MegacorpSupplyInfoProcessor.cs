﻿using buildxact_supplies.Models;
using buildxact_supplies.Validators;
using buildxact_supplies.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace buildxact_supplies.Services
{
    public class MegacorpSupplyInfoProcessor : ISupplyInfoProcessor
    {
        private readonly string fileName;
        private readonly decimal exchangeRate;
        private readonly IEnumerable<IValidationRules> validationRules;

        public MegacorpSupplyInfoProcessor(string fileName, decimal exchangeRate, IEnumerable<IValidationRules> validationRules)
        {
            this.fileName = string.IsNullOrEmpty(fileName) ? throw new ArgumentException("provide valid file name") : fileName;
            this.exchangeRate = exchangeRate <= 0 ? throw new ArgumentException("Must be larger than 0") : exchangeRate;
            this.validationRules = validationRules;
        }

        public IEnumerable<SupplyViewModel> ProcessSupplyInformation()
        {
            foreach (var validationRule in validationRules)
            {
                if (!validationRule.Valid())
                {
                    throw new ValidationException("A validation error occured!");
                }
            }

            var fileContent = Helper.SearchFileInAssemblyByName(fileName);
            if (string.IsNullOrEmpty(fileContent))
            {
                return Enumerable.Empty<SupplyViewModel>();
            }
            var supplyData = new List<SupplyViewModel>();
            var parsedFile = JsonConvert.DeserializeObject<MegacorpJsonResponse>(fileContent);
            foreach (var partner in parsedFile.partners)
            {
                foreach (var supply in partner.supplies)
                {
                    supplyData.Add(new SupplyViewModel
                    {
                        Id = supply.id,
                        ItemName = supply.description,
                        Price = supply.priceInCents > 0 ? Math.Round((supply.priceInCents * exchangeRate / 100), 2) : 0
                    });
                }
            }

            return supplyData;
        }
    }
}
