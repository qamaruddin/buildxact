﻿using buildxact_supplies.Models;
using buildxact_supplies.Validators;
using buildxact_supplies.ViewModels;
using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace buildxact_supplies.Services
{
    public class HumphriesSupplyInfoProcessor : ISupplyInfoProcessor
    {
        private readonly string fileName;
        private readonly IEnumerable<IValidationRules> validationRules;

        public HumphriesSupplyInfoProcessor(string fileName, IEnumerable<IValidationRules> validationRules)
        {
            this.fileName = string.IsNullOrEmpty(fileName) ? throw new ArgumentException("provide valid file name") : fileName;
            this.validationRules = validationRules;
        }

        public IEnumerable<SupplyViewModel> ProcessSupplyInformation()
        {
            foreach (var validationRule in validationRules)
            {
                if (!validationRule.Valid())
                {
                    throw new Validators.ValidationException("A validation error occured!");
                }
            }
            var fileContent = Helper.SearchFileInAssemblyByName(fileName);
            if (string.IsNullOrEmpty(fileContent))
            {
                return Enumerable.Empty<SupplyViewModel>();
            }
            var supplyData = new List<SupplyViewModel>();
            using (var stream = fileContent.AsStream())
            {
                using (var csvReader = new CsvReader(new StreamReader(stream), CultureInfo.InvariantCulture))
                {
                    var data = csvReader.GetRecords<HumphriesCsvResponse>();
                    foreach (var item in data)
                    {
                        supplyData.Add(new SupplyViewModel
                        {
                            Id = item.Identifier,
                            ItemName = item.Desc,
                            Price = Math.Round(item.CostAud, 2)
                        });
                    }
                }
            }

            return supplyData;
        }
    }
}
