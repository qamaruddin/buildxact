﻿using buildxact_supplies.ViewModels;
using System.Collections.Generic;

namespace buildxact_supplies.Services
{
    public interface ISupplyInfoProcessor
    {
        IEnumerable<SupplyViewModel> ProcessSupplyInformation();
    }
}
