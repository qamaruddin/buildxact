﻿using System.Collections.Generic;

namespace buildxact_supplies.Models
{
    public class MegacorpJsonResponse
    {
        public IList<Partner> partners { get; set; }
    }
    public class Supply
    {
        public string id { get; set; }
        public string description { get; set; }
        public string uom { get; set; }
        public int priceInCents { get; set; }
        public string providerId { get; set; }
        public string materialType { get; set; }
    }

    public class Partner
    {
        public string name { get; set; }
        public string partnerType { get; set; }
        public string partnerAddress { get; set; }
        public IList<Supply> supplies { get; set; }
    }
}
