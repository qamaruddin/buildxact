﻿using CsvHelper.Configuration.Attributes;

namespace buildxact_supplies.Models
{
    public class HumphriesCsvResponse
    {
        [Name("identifier")]
        public string Identifier { get; set; }
        [Name("desc")]
        public string Desc { get; set; }
        [Name("unit")]
        public string Unit { get; set; }
        [Name("costAUD")]
        public decimal CostAud { get; set; }
    }
}
